import { Component, OnInit } from '@angular/core';
import { FooterNavBarComponent } from './footer-nav-bar/footer-nav-bar.component';
import { User } from './dao/User';
import { UserService } from './dao/user.service';
import { GlobalsService } from './shared/globals.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  user: User[] = [];
  title = 'Sud-Ouest Abats';

  constructor(private userService: UserService, private globalsService : GlobalsService ) { }

  ngOnInit(): void {
    /*
    console.log("avant get utilisateur");
    if (this.globalsService.utilisateurs.length <= 0){
      console.log ("utilisateurs vide : GET");
      this.getUtilisateurs();
      console.log ("utilisateurs vide : APRES GET");
    } 
    else{
      console.log("utilisateurs pas vide " + this.globalsService.utilisateurs.length);
    }
    */
  }

  getUsers(): void {
    console.log("getUtilisateurs from app.component.ts");
    this.userService.getUsers()
    .subscribe(users => {
      this.globalsService.users = users;
      console.log("Users set. nb : " + this.globalsService.users.length);
    });
  }

}
