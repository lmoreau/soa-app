import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from "./shared/auth.guard.spec";
import { SumupComponent } from './sumup/sumup.component';

const routes: Routes = [
  { path: '', component:HomeComponent, canActivate: [AuthGuard] },
  { path: 'detail/:id', component: HeroDetailComponent },
  { path: 'sumpup', component: SumupComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
