import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroSearchComponent } from './hero-search/hero-search.component';
import { MessagesComponent } from './messages/messages.component';
import { FooterNavBarComponent } from './footer-nav-bar/footer-nav-bar.component';
import { HeaderBarComponent } from './header-bar/header-bar.component';
import { AuthComponent } from './auth/auth.component';
import { AuthInterceptor } from './shared/authconfig.interceptor';
import { HomeComponent } from './home/home.component';
import { SumupComponent } from './sumup/sumup.component'; 
 
@NgModule({ 
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,

  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroesComponent,
    HeroDetailComponent,
    MessagesComponent,
    HeroSearchComponent,
    FooterNavBarComponent,
    HeaderBarComponent,
    AuthComponent,
    HomeComponent,
    SumupComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
