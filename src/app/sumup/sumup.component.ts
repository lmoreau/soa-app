import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalsService } from '../shared/globals.service';
import { User } from '../dao/User';
import { Society } from '../dao/Society';

@Component({
  selector: 'app-sumup',
  templateUrl: './sumup.component.html',
  styleUrls: ['./sumup.component.css']
})
export class SumupComponent implements OnInit{

  currentUser : User | undefined;
  currentSociety : Society | undefined;
  
  constructor(private router: Router, private route: ActivatedRoute, private globalsService : GlobalsService) { }
 
  ngOnInit() {
    this.currentUser = this.globalsService.selectedUser;
    this.currentSociety = this.globalsService.selectedSociety;
  }

}
