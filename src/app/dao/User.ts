import { Society } from "./Society";

export interface User {
  id: number;
  lastName: string;
  firstName: string;
  status : number;
  isAdmin:boolean;
  phone:string;
  email:string;
  societies: Society[];
}