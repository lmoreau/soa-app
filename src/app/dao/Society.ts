export interface Society {
  id: number;
  code2b: string;
  callName: string;
  societyName:string;
  address:string;
}