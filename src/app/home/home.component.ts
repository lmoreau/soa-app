import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { GlobalsService } from '../shared/globals.service';
import { UserService } from '../dao/user.service';
import { User } from '../dao/User';
import { SumupComponent } from '../sumup/sumup.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit{
  
 

  constructor(private router: Router, private route: ActivatedRoute, private globalsService : GlobalsService, private userService: UserService) { }
 
  users: User[] = [];
  currentUser : User | undefined;

 clientId :  string = "";
 clientName: string = "";
 isDataLoaded : boolean = false;
 isUserFound : boolean = false;
 mustSelectSociety : boolean = false;


  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.clientId = params["clientId"];
        this.clientName = params["clientName"];
        console.log("client id : " + this.clientId);
        if (this.globalsService.users.length == 0){
          this.getUsers();
        }
        else{
          this.isDataLoaded = true;
          this.globalsService.users.forEach((currentValue, index) => {
            if(currentValue.id.toString() ==  this.clientId) {
               this.currentUser = currentValue;
               this.isUserFound = true;
               if (this.currentUser.societies.length == 1){
                this.mustSelectSociety = false;
                this.globalsService.selectedUser= this.currentUser;
                this.globalsService.selectedSociety = this.currentUser.societies[0];
                this.router.navigateByUrl('/sumpup');
               }
               else{
                this.mustSelectSociety = true;
               }
            }
          });
        }
       
        

      }
    );
  }

  getUsers(): void {
    console.log("getUsers from app.component.ts");
    this.userService.getUsers()
    .subscribe(users => {
      this.globalsService.users = users;
      console.log("utilisateurs set. nb : " + this.globalsService.users.length);
      if (this.globalsService.users.length > 0){
        this.isDataLoaded = true;
        this.globalsService.users.forEach((currentValue, index) => {
          if(currentValue.id.toString() ==  this.clientId) {
             this.currentUser = currentValue;
             this.isUserFound = true;
             if (this.currentUser.societies.length == 1){
              this.mustSelectSociety = false;
              this.globalsService.selectedUser= this.currentUser;
              this.globalsService.selectedSociety = this.currentUser.societies[0];
              this.router.navigateByUrl('/sumpup');
             }
             else{
              this.mustSelectSociety = true;
             }
          }
        });
      }
    });
  }




}
