import { Injectable } from '@angular/core' 
import { User } from '../dao/User';
import { Society } from '../dao/Society';


@Injectable({
    providedIn: 'root'
  })

export class GlobalsService { 
    public users: User[] = [];
    public selectedSociety : Society | undefined;
    public selectedUser : User | undefined;
 }